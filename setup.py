#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from setuptools import setup, find_packages
import os
import io


def read(fname):
    return io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()


setup(name='p4atasks',
    version='0.0.1',
    description='Python for android invoke tasks specification',
    long_description=read('README.md'),
    author='Datalife',
    author_email='info@datalifeit.es',
    url='http://www.datalifeit.es',
    download_url="https://gitlab.com/datalifeit/p4atasks",
    packages=find_packages(),
    package_data={},
    scripts=[],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: No Input/Output (Daemon)',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Software Development :: Libraries',
        ],
    license='GPL-3',
    install_requires=[
        'invoke',
        ],
    extras_require={},
    zip_safe=False,
    )
