# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from invoke import task
from subprocess import call
from configparser import ConfigParser
from os.path import exists
import os
import logging
import shlex

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
logger.addHandler(ch)


def _read_configuration():
    # search for a .p4a configuration file in the current directory
    if not exists(".p4a"):
        return
    logger.info("... Reading .p4a configuration")
    with open(".p4a") as fd:
        lines = fd.readlines()
    lines = [shlex.split(line) for line in lines if not line.startswith("#")]
    opts = {}
    args_ = ('--android-api', '--storage-dir', '--dist-name', '--arch',
        '--local-recipes')
    for line in lines:
        for par in line:
            for arg_ in args_:
                if par.startswith(arg_):
                    opts[arg_] = par[len(arg_) + 1:]
    return opts


def _get_apk_name(apk_name, config):
    if apk_name is not None:
        return apk_name
    return config.get('apk', 'name')


@task(help={'project': "Project name",
            'apk_name': "Name of the apk name build",
            'api': "Android API",
            'arch': "Architecture for which apk is built",
            'distribution': "Distribution to use/create",
            'clean_dists': "Clean all distribution",
            'clean_builds': "Clean all builds",
            'install': "Install apk file in current active device"})
def apk(ctx, project, apk_name=None, api=None, arch='armeabi-v7a',
        distribution=None, clean_dists=False, clean_builds=False,
        install=False):
    """Build project apk file"""

    logger.info('APK Building ...')
    opts = _read_configuration()

    cp = ConfigParser()
    cp.read('./%s/setup.cfg' % project)

    apk_name = _get_apk_name(apk_name, cp) or project
    logger.info('... APK name: %s' % apk_name)
    api = api or opts.get('--android-api', os.environ.get('ANDROIDAPI', None))
    version = cp.get('apk', 'version') or '1.0'

    apk_full_name = '%s_%s_%s-%s-debug.apk' % (apk_name, arch, api, version)
    logger.info('... APK full name: %s' % apk_full_name)

    if distribution is None:
        distribution = '%s_%s_%s' % (project, arch, api)
    logger.info('... Using/creating distribution: %s' % distribution)

    storage_dir = opts.get('--storage-dir',
        os.environ.get('P4A_STORAGE_DIR', None))
    logger.info('... storage dir: %s' % storage_dir)

    if clean_dists:
        logger.info('... Distribution will be deteled')
        clean_dists_cmd = ['p4a', 'clean_dists']
        if storage_dir:
            clean_dists_cmd.append('--storage-dir=%s' % storage_dir)
        call(clean_dists_cmd)

    if clean_builds:
        logger.info('... Builds will be deteled')
        clean_builds_cmd = ['p4a', 'clean_builds']
        if storage_dir:
            clean_builds_cmd.append('--storage-dir=%s' % storage_dir)
        call(clean_builds_cmd)

    local_recipes = opts.get('--local-recipes',
        os.environ.get('P4A_LOCAL_RECIPES', None))
    logger.info('... Local recipes: %s' % local_recipes)

    setup_cmd = [
        'python3', 'setup.py', 'apk',
        '--dist-name=%s' % distribution,
        '--arch=%s' % arch]
    if api is not None:
        setup_cmd.extend([
            '--android-api=%s' % api,
            '--sdk=%s' % api])
    if storage_dir:
        setup_cmd.extend([
            '--storage-dir=%s' % storage_dir])
    if local_recipes:
        setup_cmd.extend([
            '--local-recipes=%s' % local_recipes])
    os.chdir(project)
    result = call(setup_cmd)
    if result:
        exit(result)

    os.rename('%s-%s-debug.apk' % (apk_name.replace(' ', ''),
        version), apk_full_name)
    if install:
        logger.info('... APK Install')
        call(['adb', 'install', '-r', apk_full_name])


@task(help={'project': "Project name",
            'apk_name': "Name of the apk name build",
            'api': "Android API",
            'arch': "Architecture for which apk is built"})
def install(ctx, project, apk_name=None, api='21', arch='armeabi-v7a'):
    """Install project apk file"""

    logger.info('APK Install ...')
    cp = ConfigParser()
    cp.read('./%s/setup.cfg' % project)

    apk_name = _get_apk_name(apk_name, cp) or project
    version = cp.get('apk', 'version') or '1.0'
    logger.info('... APK name: %s' % apk_name)
    apk_full_name = '%s_%s_%s-%s-debug.apk' % (apk_name, arch, api, version)
    logger.info('... APK full name: %s' % apk_full_name)

    os.chdir(project)
    call(['adb', 'install', '-r', apk_full_name])


def _get_modules_path():
    import sys
    end_str = '/site-packages'
    mod_path = None
    mod_init = None
    for p in sys.path:
        if (p.endswith(end_str)):
            mod_init = p + '/trytond/modules/__init__'
            break
    for f_ext in ('py', 'pyc', 'pyo'):
        mod_path = '%s.%s' % (mod_init, f_ext)
        if os.path.exists(mod_path):
            return os.path.dirname(mod_path)
    return None


@task()
def create_links(ctx):
    IGNORE_REPOS = ['trytond', 'tryton', 'sao', 'proteus',
                'dvconfig', 'config', 'nereid']
    res = True
    trytond_mod_path = _get_modules_path()
    if not trytond_mod_path:
        raise Exception('Tryton modules path not found')
    print('Trytond modules path: %s' % trytond_mod_path)
    work_dir_path = os.getcwd()
    print('Working folder path: %s' % work_dir_path)

    cp = ConfigParser()
    cp.read('config/own.cfg', encoding='utf8')
    repos_ = cp.sections()
    for sect in repos_:
        if sect in IGNORE_REPOS:
            continue
        dest_path = '%s/%s' % (trytond_mod_path, sect)
        src_path = '%s/%s/%s' % (work_dir_path, cp.get(sect, 'path'), sect)
        print(dest_path)
        print(src_path)
        if not os.path.exists(dest_path):
            print(('Creating symbolic link of "%s"' % sect))
            if os.path.exists(src_path):
                print(os.popen('ln -s -f %s %s' % (src_path,
                    trytond_mod_path)).read())
                res = True
            else:
                raise Exception('Source code not found for module %s' % sect)

    return res
